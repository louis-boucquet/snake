import sys, time
from pynput.keyboard import Key, Listener
import cli

def read_scores():
	try:
		with open("./scores") as scores:
			return list(name.strip() for name in scores)
	except:
		add_scores("First", 0)
		return []

def add_scores(user, score):
	with open("./scores", "a") as scores_file:
		scores_file.write(f"{user} {score} {time.time()}\n")

read_scores()
