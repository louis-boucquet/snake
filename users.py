import sys
from pynput.keyboard import Key, Listener
import cli

def read_users():
	try:
		with open("./users") as users:
			return set(name.strip() for name in users)
	except:
		write_users(set())
		return set()

def write_users(users):
	with open("./users", "w") as users_file:
		users_file.write("\n".join(users))

class UserSelecter:
	def __init__(self, users):
		self.users = list(users)
		self.selected = 0
		self.listener = None

	def print_menu(self):
		cli.move_to((0, 0))

		sys.stdout.write(cli.White)
		cli.print_in_box("Play as:")

		for i, user in enumerate(self.users):
			sys.stdout.write(cli.Magenta if i == self.selected else cli.White)
			sys.stdout.write(f"• {user}\n")

	def on_press(self, key):
		if key == Key.down:
			self.selected += 1
		elif key == Key.up:
			self.selected -= 1
		elif key == Key.enter:
			self.listener.stop()

		self.selected %= len(self.users)

		self.print_menu()

	def select_user(self):
		with Listener(on_press=self.on_press) as listener:
			self.listener = listener
			self.selected = 0
			cli.clear()

			self.print_menu()

			listener.join()

		return self.users[self.selected]

users = read_users()
selecter = UserSelecter(users)
