import sys, time
from Position import Position
from Snake import Snake
from Food import Food
from pynput.keyboard import Key, Listener

def game():
	size = (29, 15)
	food = Food(Position(size))
	snake = Snake(food, Position((5, 0)), Position((0, 1)), Position(size), 6)
	running = True

	def on_press(key):
		if key == Key.left:
			snake.direction = (-1, 0)
		elif key == Key.right:
			snake.direction = (1, 0)
		elif key == Key.up:
			snake.direction = (0, -1)
		elif key == Key.down:
			snake.direction = (0, 1)

	def on_release(key):
		if key == Key.esc:
			running = False
			return False

	def print_grid(size):
		x, y = size

		start = "╭" + "┬".join("───" for _ in range(x)) + "╮"
		end = "╰" + "┴".join("───" for _ in range(x)) + "╯"
		row = "│" + "│".join("   " for _ in range(x)) + "│"
		sep = "├" + "┼".join("───" for _ in range(x)) + "┤"

		body = f"\n{sep}\n".join(row for _ in range(y))

		sys.stdout.write(f"\x1b[1;30m")
		sys.stdout.write(f"{start}\n{body}\n{end}")

	with Listener(
			on_press=on_press,
			on_release=on_release
	) as listener:

		sys.stdout.write(f"\x1b[2J")
		sys.stdout.write(f"\x1b[0;0f")
		print_grid(size)

		while snake.is_alive():
			snake.print()
			food.print()

			sys.stdout.write(f"\x1b[0;0f")

			sys.stdout.flush()
			time.sleep(food.time)

			snake.move()

		# clear the screen
		sys.stdout.write(f"\x1b[2J")
		sys.stdout.write(f"\x1b[0;0f")

	return food.score
