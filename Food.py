import random, sys
from Position import Position

class Food:
	def __init__(self, grid_size):
		self.grid_size = grid_size
		self.position = Position(map(random.randrange, self.grid_size))
		self.score = 0
		self.time = 0.5

	def spawn_random(self):
		self.score += 1
		self.time *= 0.95
		self.position = Position(map(random.randrange, self.grid_size))

	def __str__(self):
		return str(self.position)

	def print(self):
		x, y = self.position
		sys.stdout.write(f"\x1b[0;31m\x1b[{y * 2 + 1 + 1};{x * 4 + 2 + 1}f\xF0")
