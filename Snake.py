import sys

class Snake:
	def __init__(self, food, start, curr_direction, world_size, length = 4):
		self.food = food
		self.snake = [start]
		self.curr_direction = curr_direction
		self.direction_buffer = []
		self.world_size = world_size

		for _ in range(length - 1):
			self.expand()

	@property
	def direction(self):
		return self.curr_direction

	@direction.setter
	def direction(self, value):
		if len(self.direction_buffer) < 2:
			self.direction_buffer.append(value)
		else:
			self.direction_buffer[1] = value

	def move_buffer(self):
		if self.direction_buffer:
			self.curr_direction = self.direction_buffer[0]
			self.direction_buffer = self.direction_buffer[1:]

	@property
	def head(self):
		return self.snake[-1]

	def move(self):
		self.expand()
		if self.head == self.food.position:
			self.food.spawn_random()
		else:
			self.cut_tail()

	def is_alive(self):
		return all(self.head != s for s in self.snake[:-1]) \
			and self.head.x >= 0 and self.head.y >= 0 \
			and self.head.x < self.world_size.x and self.head.y < self.world_size.y

	def expand(self):
		self.move_buffer()
		self.snake.append(self.head + self.direction)

	def cut_tail(self):
		x, y = self.snake[0]
		self.snake = self.snake[1:]

		sys.stdout.write(f"\x1b[0;32m\x1b[{y * 2 + 1 + 1};{x * 4 + 2 + 1}f ")

	def print(self):
		for x, y in self.snake:
			sys.stdout.write(f"\x1b[1;37m\x1b[{y * 2 + 1 + 1};{x * 4 + 2 + 1}f#")

		x, y = self.head
		sys.stdout.write(f"\x1b[0;32m\x1b[{y * 2 + 1 + 1};{x * 4 + 2 + 1}f#")
