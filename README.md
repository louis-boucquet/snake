# Snake

I made a cli version of the classic game "snake".  
The game uses [ansi escape characters](https://bluesock.org/~willkg/dev/ansi.html) to print in colors or at specifiek positions in the terminal.  
Also noteworthy are the ["box" unicode characters](https://www.wikiwand.com/en/Box-drawing_character) I used to print the grid.

![screenshot](./Screen.png)
