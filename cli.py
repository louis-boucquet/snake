import sys

Black = "\x1b[30m"
Red = "\x1b[31m"
Green = "\x1b[32m"
Yellow = "\x1b[33m"
Blue = "\x1b[34m"
Magenta = "\x1b[35m"
Cyan = "\x1b[36m"
White = "\x1b[37m"
Reset = "\x1b[0m"

def clear():
	sys.stdout.write(f"\x1b[2J")

def move_to(position):
	x, y = position
	sys.stdout.write(f"\x1b[{x};{y}f")

def print_in_box(text):
	sys.stdout.write(f"╭" + "─" * (2 + len(text)) + "╮\n")
	sys.stdout.write(f"│ {text} │\n")
	sys.stdout.write(f"╰" + "─" * (2 + len(text)) + "╯\n")
