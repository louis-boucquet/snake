from operator import add

class Position(tuple):
	def __add__(self, other):
		return Position(map(add, self, other))
	
	@property
	def x(self):
		return self[0]
	
	@property
	def y(self):
		return self[1]
